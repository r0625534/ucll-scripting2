import argparse
import qrcode
import sys



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data', type=str, required=True)
    parser.add_argument('-o', '--output', type=str, required=True)
    args = parser.parse_args()

    img = qrcode.make(args.data)
    img.save(args.output)

    
