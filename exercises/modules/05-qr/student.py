'''
Schrijf een script dat een QR-code genereert.

Het script krijgt als command line arguments:

  -d DATA of --data DATA
      String die moet omgezet worden

  -o FILE of --output FILE
      Bestand waarnaar de QR code moet geschreven worden.


Bijvoorbeeld,

   python student -d "Hello world" -o "hello-world.png"

moet het bestand "hello-world.png" genereren met een QR-code
in dat "Hello world" encodeert.
'''
