import argparse
import cv2
import os


def count_faces(file):
    # Pad moet aangepast worden
    face_cascade = cv2.CascadeClassifier("D:/Python36/Lib/site-packages/cv2/data/haarcascade_frontalface_default.xml")

    img = cv2.imread(file)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    
    return len(faces)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('files', metavar='FILES', nargs='+')
    args = parser.parse_args()

    for file in args.files:
        print(f"{file} {count_faces(file)}")
