'''
Schrijf een script dat men vanuit de shell kan opstarten
om het aantal gezichten te tellen in grafische bestanden.
Per gegeven bestand print het de bestandsnaam af gevolgd
door het aantal gezichten.

Bv.

    python student.py *.jpg

geeft

    no-faces.jpg 0
    3-faces.jpg 3
    16-faces.jpg 16
'''
