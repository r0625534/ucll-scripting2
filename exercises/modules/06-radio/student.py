'''
Write a Python application that creates a web server using Flask.
If you go to http://localhost:5000, you should get a list
of all mp3's in the current directory.
Clicking on any of these mp3's should make the music play.
'''
