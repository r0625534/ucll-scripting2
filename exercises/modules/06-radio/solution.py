from flask import Flask, redirect
import os
from pygame import mixer


app = Flask(__name__)
mixer.init()



@app.route("/")
def show_files():
    items = "\n".join( f'<li><a href="play/{file}">{file}</li>' for file in os.listdir('.') if file.endswith('mp3') )
    result = f'<html><body><ul>{items}</ul></body></html>'
    return result


@app.route("/play/<file>")
def play(file):
    mixer.music.load(file)
    mixer.music.play()
    return redirect('/')
