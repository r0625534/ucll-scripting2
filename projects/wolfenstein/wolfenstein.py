import math


class Vector2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def __add__(self, other):
        return Vector2D(self.x + other.x, self.y + other.y)

    def __mul__(self, factor):
        return Vector2D(self.x * factor, self.y * factor)

    def __repr__(self):
        return f"({self.x}, {self.y})"

class Ray:
    def __init__(self, origin, direction):
        self.origin = origin
        self.direction = direction

class Grid:
    def __init__(self, width, height):
        self.__grid = [ [ False for _ in range(height) ] for _ in range(width) ]
        self.width = width
        self.height = height
    
    def __vertical_intersection(self, ray):
        if ray.direction.x < 0:
            xs = range(math.floor(ray.origin.x), -1, -1)
        elif ray.direction.x > 0:
            xs = range(math.ceil(ray.origin.x), self.width + 1)

        for x in xs:
            t = (x - ray.origin.x) / ray.direction.x
            y = ray.origin.y + ray.direction.y * t
            yield ( Vector2D(x, y), t )

    def __horizontal_intersection(self, ray):
        if ray.direction.y < 0:
            ys = range(math.floor(ray.origin.y), -1, -1)
        elif ray.direction.y > 0:
            ys = range(math.ceil(ray.origin.y), self.height + 1)

        for y in ys:
            t = (y - ray.origin.y) / ray.direction.y
            x = ray.origin.x + ray.direction.x * t
            yield ( Vector2D(x, y), t )

    

g = Grid(5, 5)
r = Ray( Vector2D(0.5, 0.5), Vector2D(0, 0.2) )
print(list(g.horizontal_intersection(r)))