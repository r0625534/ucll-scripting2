require 'rubygems'
require 'bundler/setup'

require 'erb'
require 'diff'


class Context
  def section_header(title)
    <<-END
    <header>
      <h1>#{title}</h1>
    </header>
    END
  end

  def git(name)
    <<-END
    <div class="git">#{name}</div>
    END
  end

  def diff(dirname)
    src1 = IO.read("code/#{dirname}/original").lines.map(&:chomp)
    src2 = IO.read("code/#{dirname}/updated").lines.map(&:chomp)

    html = Diff.diff(src1, src2).map do |line|
      html_class = line.operation.to_s
      <<-END
<span class="#{html_class}">#{line.contents.gsub(/ /, '&nbsp;')}</span>
      END
    end.join

    <<-END
    <div class="code">#{html}</div>
    END
  end

  def grid_svg
    (0...64).map do |x|
      (0...48).map do |y|
        %{<rect x="#{10 * x + 1}" y="#{10 * y + 1}" width="8" height="8" style="fill: white;" />}
      end.join("\n")
    end.join("\n")
  end
end


template = IO.read('assignment.html.template')
erb = ERB.new template
context = Context.new
contents = erb.result(context.instance_eval { binding })
IO.write('assignment.html', contents)
