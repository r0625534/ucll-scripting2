import sys
import pygame
import random


class Position:
    def __init__(self, x, y):
        self.__x = x
        self.__y = y

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __add__(self, other):
        return Position(self.x + other.x, self.y + other.y)


class GridBase:
    @property
    def size(self):
        return (self.width, self.height)

    def is_valid_position(self, position):
        return 0 <= position.x and position.x < self.width and 0 <= position.y and position.y < self.height

    @property
    def positions(self):
        return [ Position(x, y) for x in range(0, self.width) for y in range(0, self.height) ]

    def window(self, position, size):
        return VirtualGrid(self, position, size)

    def rotate_cw(self):
        return Grid(self.height, self.width, lambda p: self[Position(p.y, self.height - p.x - 1)])

    def rotate_ccw(self):
        return self.rotate_cw().rotate_cw().rotate_cw()


class Grid(GridBase):
    def __init__(self, width, height, initializer = None):
        initializer = initializer or (lambda _: None)
        self.__items = [ [ initializer(Position(x, y)) for y in range(0, height) ] for x in range(0, width) ]

    def __repr__(self):
        return repr(self.__items)

    def __getitem__(self, position):
        return self.__items[position.x][position.y]

    def __setitem__(self, position, value):
        self.__items[position.x][position.y] = value

    @property
    def width(self):
        return len(self.__items)

    @property
    def height(self):
        return len(self.__items[0])



class VirtualGrid(GridBase):
    def __init__(self, grid, position, size):
        self.__grid = grid
        self.__position = position
        self.__size = size

    @property
    def width(self):
        width, _ = self.__size
        return width

    @property
    def height(self):
        _, height = self.__size
        return height

    def is_valid_position(self, position):
        return self.__grid.is_valid_position(position + self.__position) and super().is_valid_position(position)

    def __getitem__(self, position):
        if self.is_valid_position(position):
            return self.__grid[self.__position + position]
        else:
            raise Exception("Invalid position")

    def __setitem__(self, position, value):
        if self.is_valid_position(position):
            self.__grid[self.__position + position] = value
        else:
            raise Exception("Invalid position")



def draw_grid(surface, grid, position):
    dx, dy = position.x, position.y

    for x in range(0, grid.width):
        for y in range(0, grid.height):
            if grid[Position(x, y)]:
                px = (x + dx) * BLOCK_SIZE
                py = (y + dy) * BLOCK_SIZE
                pygame.draw.rect(surface, WHITE, [px + 1, py + 1, BLOCK_SIZE - 2, BLOCK_SIZE - 2])



class Pit:
    def __init__(self, width, height):
        self.__grid = Grid(width, height, lambda x: None)

    @property
    def grid(self):
        return self.__grid

    def fits(self, shape, position):
        window = self.__grid.window(position, shape.grid.size)

        def fits_at(position):
            if shape.grid[position]:
                return window.is_valid_position(position) and not window[position]
            else:
                return True

        return all(fits_at(position) for position in window.positions)

    def insert(self, shape, position):
        window = self.__grid.window(position, shape.grid.size)

        for position in window.positions:
            if shape.grid[position]:
                window[position] = shape.grid[position]

    def __remove_row(self, row):
        for y in range(row, 0, -1):
            for x in range(0, self.__grid.width):
                self.__grid[Position(x, y)] = self.__grid[Position(x, y-1)]

    def __is_full_row(self, row):
        return all(self.__grid[Position(x, row)] for x in range(0, self.__grid.width))

    def remove_full_rows(self):
        for y in range(0, self.__grid.height):
            if self.__is_full_row(y):
                self.__remove_row(y)



class Shape:
    @staticmethod
    def parse(string):
        lines = [ line.strip() for line in string.strip().split("\n") ]
        width = len(lines[0])
        height = len(lines)
        grid = Grid(width, height, lambda p: lines[p.y][p.x] == "*")
        return Shape(grid)

    @staticmethod
    def O():
        return Shape.parse("""
                           **
                           **
                           """)

    @staticmethod
    def I():
        return Shape.parse("""
                           ****
                           """)

    @staticmethod
    def T():
        return Shape.parse("""
                           .*.
                           ***
                           """)

    @staticmethod
    def L():
        return Shape.parse("""
                           ..*
                           ***
                           """)

    @staticmethod
    def J():
        return Shape.parse("""
                           ***
                           ..*
                           """)

    @staticmethod
    def S():
        return Shape.parse("""
                           .**
                           **.
                           """)

    @staticmethod
    def Z():
        return Shape.parse("""
                           **.
                           .**
                           """)

    @staticmethod
    def shapes():
        return [ Shape.T(),
                 Shape.O(),
                 Shape.S(),
                 Shape.Z(),
                 Shape.I(),
                 Shape.L(),
                 Shape.J() ]

    @staticmethod
    def random():
        return random.choice( Shape.shapes() )

    def __init__(self, grid):
        self.__grid = grid

    @property
    def grid(self):
        return self.__grid

    def rotate_cw(self):
        return Shape(self.grid.rotate_cw())

    def rotate_ccw(self):
        return Shape(self.grid.rotate_ccw())


class Game:
    def __init__(self, width = 10, height = 20):
        self.__pit = Pit(width, height)
        self.__next_shape()
    
    @property
    def pit(self):
        return self.__pit

    @property
    def shape(self):
        return self.__shape

    @property
    def shape_position(self):
        return self.__shape_position

    def move_shape_left(self):
        self.__move_shape(Position(-1, 0))

    def move_shape_right(self):
        self.__move_shape(Position(1, 0))

    def __move_shape(self, delta):
        updated_position = self.__shape_position + delta

        if self.__pit.fits(self.__shape, updated_position):
            self.__shape_position = updated_position
            return True
        else:
            return False

    def rotate_shape_cw(self):
        rotated = self.__shape.rotate_cw()

        if self.__pit.fits(rotated, self.__shape_position):
            self.__shape = rotated

    def rotate_shape_ccw(self):
        rotated = self.__shape.rotate_ccw()

        if self.__pit.fits(rotated, self.__shape_position):
            self.__shape = rotated

    def move_shape_down(self):
        if not self.__move_shape(Position(0,1)):
            self.__freeze_shape()
            self.__pit.remove_full_rows()
            self.__next_shape()
            return False
        else:
            return True

    def __freeze_shape(self):
        self.__pit.insert(self.__shape, self.__shape_position)

    def drop_shape(self):
        while self.move_shape_down():
            pass

    def __next_shape(self):
        self.__shape = Shape.random()
        self.__shape_position = Position(self.pit.grid.width // 2, 0)


pygame.init()


BLOCK_SIZE = 32
PIT_WIDTH = 10
PIT_HEIGHT = 20
SCREEN_SIZE = (PIT_WIDTH * BLOCK_SIZE, PIT_HEIGHT * BLOCK_SIZE)
SCREEN = pygame.display.set_mode(SCREEN_SIZE)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)


def gameloop():
    game = Game()

    key_bindings = {}
    key_bindings[pygame.K_LEFT] = lambda: game.move_shape_left()
    key_bindings[pygame.K_RIGHT] = lambda: game.move_shape_right()
    key_bindings[pygame.K_UP] = lambda: game.rotate_shape_ccw()
    key_bindings[pygame.K_DOWN] = lambda: game.move_shape_down()
    key_bindings[pygame.K_SPACE] = lambda: game.drop_shape()


    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key in key_bindings:
                    key_bindings[event.key]()
                

        SCREEN.fill(BLACK)

        draw_grid(SCREEN, game.pit.grid, Position(0, 0))
        draw_grid(SCREEN, game.shape.grid, game.shape_position)

        pygame.display.flip()


gameloop()